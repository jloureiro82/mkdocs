# Funções Úteis

## Funcões matemáticas

```python hl_lines="2"
def soma(a: int, b: int): -> int
    return a + b
```

```python hl_lines="2"
def subtracao(a: int, b: int): -> int
    return a - b
```

```python hl_lines="2"
def multiplicacao(a: int, b: int): -> int
    return a * b
```

```python hl_lines="2"
def divisao(a: int, b: int): -> int
    return a / b
```
## Funcões aleatórias 